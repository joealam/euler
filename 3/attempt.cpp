#include <iostream>
#include <climits>

using namespace std;

const long MAX = LONG_MAX;

bool isPrime( long );
long nextPrime( long );

long nextPrime( long start )
{
	int i = start + 1;
	while ( i < MAX )
	{
		if ( isPrime( i ) ) return i;
		i++;
	}
	return -1;
}

bool isPrime( long n )
{
	for ( int i = 2; i < n; i++ )
	{
		if ( ( n % i ) == 0 )
		{
			return false;
		}
	}
	return true;
}

int main( int argc, char* argv[] )
{
	long number = 600851475143;
	long i = 2;
	
	while ( true )
	{
		if ( (number % i) == 0 )
		{
			cout << "\r" << number/i << "\t\t\t";
			if ( isPrime( number / i ) )
			{
				cout << "Result: " << number / i;
				break;
			}
			else
			{
				number = number / i;
				i = 1;
			}
		}
		i = nextPrime( i );
	}

	return 0;
}
