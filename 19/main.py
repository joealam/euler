# Project Euler Problem #19
# How many Sundays have occurred on the first of the month during the
# twentieth century (1901-2000, inclusive)

def IsLeapYear(year):
    return ((year % 4 == 0) and (year % 100 != 0 or year % 400 == 0))

daysInMonth = [
    31, # January
    28, # February
    31, # March
    30, # April
    31, # May
    30, # June
    31, # July
    31, # August
    30, # September
    31, # October
    30, # November
    31, # December
    ]

count = 0
firstDayInMonth = 0 # 1st Jan 1901
lastSundayOnFirst = 1 # 2nd Jan 1901 is a Sunday
for year in range(1901, 2000):
    for month in range(0, 12):
        if (month == 1 and IsLeapYear(year)): # February
            firstDayInMonth += 29
        else:
            firstDayInMonth += daysInMonth[month]

        if ((firstDayInMonth - lastSundayOnFirst) % 7 == 0):
            count += 1
            lastSundayOnFirst = firstDayInMonth

print count