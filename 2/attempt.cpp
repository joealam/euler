#include <iostream>

using namespace std;

const int MAX = 4000000;

int main( int argc, char* argv[] )
{
	int sum = 0;
	int current = 1;
	int prev = 0;

	while ( ( current += prev ) < MAX )
	{
		prev = current - prev;
		if ( ( current % 2 ) == 0 )
		{
			cout << "+" << current << " = " << (sum += current) << endl;
		}
	}

	cout << "Total: " << sum << endl;

	return 0;
}
