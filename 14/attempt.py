import sys

max = 1000000;
min = 1;

i = max;
highestCount = 0;
highest = 0;

while ( i > min ):
	sys.stdout.write( '\r' );
	sys.stdout.write( str(i) );
	n = i;
	count = 1;

	while( n > 1 ):
		#print n

		if ( n % 2 == 0 ):
			# Even
			n = n / 2
		else:
			# Odd
			n = (3 * n) + 1
		count += 1;
	if ( count > highestCount ):
		highestCount = count;
		highest = i;
	i -= 1;
print "Highest: ", highest
