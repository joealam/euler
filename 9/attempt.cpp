#include <iostream>
#include <cmath>

using namespace std;

int main( int argc, char* argv[] )
{
	const int target = 1000;
	const int max_try = target;

	for ( int a = 1; a < max_try; a++ )
	{
		for ( int b = a + 1; b < max_try; b++ )
		{
			float c = sqrt( pow( a, 2 ) + pow( b, 2 ) );
			if ( c == (int)c )
			{
				if ( a + b + c == target )
				{
					cout << a << ", " << b << ", " << c << " add together to make " << target << endl;
					cout << "Product: " << (int)(a*b*c) << endl;
				}
			}
		}
	}
	
	return 0;
}
