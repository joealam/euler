#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>

using namespace std;

string toString( long n )
{
	stringstream s;
	s << n;
	return s.str();
}

int main( int argc, char* argv[] )
{
	const int start = 999;
	const int end = 100;
	int found = 0;

	for ( int a = start; a > end; a-- )
	{
		for ( int b = start; b > end; b-- )
		{
			cout << a << " x " << b << " = " << a*b << endl;
			string product = toString(a * b);
			string reversed = product;
			reverse( reversed.begin(), reversed.end() );
			if ( product == reversed )
			{
				if ( (a*b) > found )
				{
					found = a*b;
				}
			}
		}
	}
	cout << "The solution is " << found << endl;
	
	return 0;
}
