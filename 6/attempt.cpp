#include <iostream>

using namespace std;

int main( int argc, char* argv[] )
{
	const int upto = 100;

	// Calculate square of the sum
	int sum = 0;
	for ( int i = 0; i <= upto; i++ )
	{
		sum += i;
	}
	int sum_sq = sum * sum;

	// Calculate the sum of the squares
	int sq_sum = 0;
	for ( int i = 0; i <= upto; i++ )
	{
		sq_sum += i * i;
	}

	// Calculate the difference
	cout << sum_sq << " - " << sq_sum << " = " << sum_sq - sq_sum << endl;

	return 0;
}
