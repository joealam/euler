#include <iostream>

using namespace std;

int main( int argc, char* argv[] )
{
	int i = 0;
	int sum = 0;
	const int max = 1000;

	while ( (i += 3) < max )
	{
		sum += i;
		cout << "Added " << i << endl;
	}

	i = 0;
	while ( (i += 5) < max )
	{
		if ( (i % 3) != 0 )
		{
			sum += i;
			cout << "Added " << i << endl;
		}
	}

	cout << sum << endl;
	return 0;
}
