/* Project Euler Problem 11
 */
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <algorithm>

using namespace std;

const int GRID_SIZE = 20;

void LoadGrid( int** grid, const char* filename );

// Functions for calculate the sum in a direction
int CalculateUp( int x, int y, int** grid );
int CalculateDown( int x, int y, int** grid );
int CalculateLeft( int x, int y, int** grid );
int CalculateRight( int x, int y, int** grid );
int CalculateUpLeft( int x, int y, int** grid );
int CalculateUpRight( int x, int y, int** grid );
int CalculateDownLeft( int x, int y, int** grid );
int CalculateDownRight( int x, int y, int** grid );

void OutputGrid( int** grid );

int main( int argc, char* argv[] )
{
	const char* filename = "grid.txt";

	int** grid = new int*[GRID_SIZE];
	for ( int y = 0; y < GRID_SIZE; y++ )
	{
		grid[y] = new int[GRID_SIZE];
		for ( int x = 0; x < GRID_SIZE; x++ )
		{
			grid[y][x] = 0;
		}
	}

	LoadGrid( grid, filename );

	cout << "Using grid: " << endl;
	OutputGrid( grid );
	cout << endl;

	int largest = 0;

	for ( int y = 0; y < GRID_SIZE; y++ )
	{
		for ( int x = 0; x < GRID_SIZE; x++ )
		{
			cout << "(" << x << ", " << y << "):";
			cout << "\t" << (largest = max( largest, CalculateUp( x, y, grid ) ));
			cout << "\t" << (largest = max( largest, CalculateDown( x, y, grid ) ));
			cout << "\t" << (largest = max( largest, CalculateLeft( x, y, grid ) ));
			cout << "\t" << (largest = max( largest, CalculateRight( x, y, grid ) ));
			cout << "\t" << (largest = max( largest, CalculateUpLeft( x, y, grid ) ));
			cout << "\t" << (largest = max( largest, CalculateUpRight( x, y, grid ) ));
			cout << "\t" << (largest = max( largest, CalculateDownLeft( x, y, grid ) ));
			cout << "\t" << (largest = max( largest, CalculateDownRight( x, y, grid ) ));
			cout << endl;
		}
		cout << endl;
	}

	cout << "Largest sum: " << largest << endl;

	// Delete the grid
	for ( int y = 0; y < GRID_SIZE; y++ )
	{
		delete[] grid[y];
	}
	delete[] grid;
}


void LoadGrid( int** grid, const char* filename )
{
	ifstream infile;
	infile.open( filename );

	int y = 0;
	while( infile.good() && ! infile.eof() && y < GRID_SIZE )
	{
		string line;
		getline( infile, line );

		stringstream s;
		s << line;
		for ( int x = 0; x < GRID_SIZE; x++ )
		{
			string number;
			getline( s, number, ' ' );

			stringstream n;
			n << number;
			
			n >> grid[y][x];
		}

		y++;
	}

	infile.close();
}

// Functions for calculate the sum in a direction
int CalculateUp( int x, int y, int** grid )
{
	if ( y >= 3 )
	{
		return (	grid[y][x] *
				grid[y-1][x] *
				grid[y-2][x] *
				grid[y-3][x]
		       );
	}
	else return 0;
}

int CalculateDown( int x, int y, int** grid )
{
	if ( y <= GRID_SIZE - 4 )
	{
		return (	grid[y][x] *
				grid[y+1][x] *
				grid[y+2][x] *
				grid[y+3][x]
		       );
	}
	else return 0;
}

int CalculateLeft( int x, int y, int** grid )
{
	if ( x >= 3 )
	{
		return (	grid[y][x] *
				grid[y][x-1] *
				grid[y][x-2] *
				grid[y][x-3]
		       );
	}
	else return 0;
}

int CalculateRight( int x, int y, int** grid )
{
	if ( y <= GRID_SIZE - 4 )
	{
		return (	grid[y][x] *
				grid[y][x+1] *
				grid[y][x+2] *
				grid[y][x+3]
		       );
	}
	else return 0;
}

int CalculateUpLeft( int x, int y, int** grid )
{
	if ( x >= 3 && y >= 3 )
	{
		return (	grid[y][x] *
				grid[y-1][x-1] *
				grid[y-2][x-2] *
				grid[y-3][x-3]
		       );
	}
	else return 0;
}

int CalculateUpRight( int x, int y, int** grid )
{
	if ( x <= GRID_SIZE - 4 && y >= 3 )
	{
		return (	grid[y][x] *
				grid[y-1][x+1] *
				grid[y-2][x+2] *
				grid[y-3][x+3]
		       );
	}
	else return 0;
}

int CalculateDownLeft( int x, int y, int** grid )
{
	if ( x >= 3 && y <= GRID_SIZE - 4 )
	{
		return (	grid[y][x] *
				grid[y+1][x-1] *
				grid[y+2][x-2] *
				grid[y+3][x-3]
		       );
	}
	else return 0;
}

int CalculateDownRight( int x, int y, int** grid )
{
	if ( x <= GRID_SIZE - 4 && y <= GRID_SIZE - 4 )
	{
		return (	grid[y][x] *
				grid[y+1][x+1] *
				grid[y+2][x+2] *
				grid[y+3][x+3]
		       );
	}
	else return 0;
}

void OutputGrid( int** grid )
{
	for ( int y = 0; y < GRID_SIZE; y++ )
	{
		for ( int x = 0; x < GRID_SIZE; x++ )
		{
			cout.fill('0');
			cout.width(2);
			cout << grid[y][x] << " ";
		}
		cout << endl;
	}
}
