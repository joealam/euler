#include <iostream>
#include <vector>

using namespace std;

void generatePrimes( vector<long>*, long );

int main( int argc, char* argv[] )
{
	const long MAX = 2000000;

	vector<long> primes;
	generatePrimes( &primes, MAX );

	long long sum = 0;
	for ( long i = 0; i < primes.size(); i++ )
	{
		cout << primes[i] << endl;
		sum += primes[i];
	}

	cout << "Final sum: " << sum << endl;

	return 0;
}

void generatePrimes( vector<long>* dest, long max )
{
	bool* marked = new bool[max];
	for ( long i = 0; i < max; i++ )
	{
		marked[i] = false;
	}

	// Check numbers
	long p = 2;
	while ( p < max )
	{
		long start_p = p;

		for ( long i = p; (p * i) < max; i++ )
		{
			//cout << "Marking: " << i << endl;
			marked[p * i] = true;
		}

		for ( long i = p; i < max; i++ )
		{
			if ( marked[i + 1] == false )
			{
				p = i + 1;
				break;
			}
		}
		if ( p == start_p ) break;
	}

	for ( long i = 2; i < max; i++ )
	{
		if ( marked[i] == false )
		{
			dest->push_back( i );
		}
	}
	delete[] marked;
}
