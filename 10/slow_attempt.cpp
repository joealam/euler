// Note: This doesn't not work because "long" cannot hold large enough numbers.
#include <iostream>
#include <climits>

using namespace std;

bool isPrime( long );
long nextPrime( long );

bool isPrime( long n )
{
	for ( int i = 2; i < n; i++ )
	{
		if ( ( n % i ) == 0 )
		{
			return false;
		}
	}
	return true;
}

long nextPrime( long start )
{
	int i = start + 1;
	while ( i < LONG_MAX )
	{
		if ( isPrime( i ) ) return i;
		i++;
	}
	return -1;
}

int main( int argc, char* argv[] )
{
	const int max_prime = 2000000;

	int current = 2;
	int sum = 0;

	while ( current < max_prime )
	{
		cout << "\r" << current;
		sum += current;

		current = nextPrime( current );
	}
	cout << "\n";

	cout << "Sum of all prime numbers under " << max_prime << " is " << sum << endl;
	
	return 0;
}
