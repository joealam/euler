#include <iostream>
#include <sstream>

using namespace std;

int main( int argc, char* argv[] )
{
	const int max = 100;

	long long factorial = 1;
	for ( int i = 1; i <= max; i++ )
	{
		factorial *= i;
		/*while ( (factorial % 100) == 0 )
		{
			factorial /= 100;
		}*/
	}

	cout << max << "! = " << factorial << endl;

	stringstream s;
	s << factorial;
	string digits = s.str();
	digits = "93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864";

	int sum = 0;
	for ( int i = 0; i < digits.length(); i++ )
	{
		cout << "+ " << digits[i] << endl;
		sum += (digits[i] - '0');
	}
	
	cout << "The sum of all the digits in " << factorial << " (" << max << "!) is " << sum << endl;

	return 0;
}
