# Project Euler Problem #15

# The concept here is to determine how many possible routes there are to reach the bottom-right cell
# of a 20x20 grid if only down and right movements are allowed.
# The number of possible moves to reach a given cell is therefore equal to the sum of the number of
# routes to the cells to the left and above it (with values in row(-1) and cell(-1) being treated as 1)

width = 20
height = 20
grid = []
for y in range(height):
    row = []
    prevRow = grid[y-1] if y > 0 else [1 for i in range(width)]
    for x in range(width):
        prevColValue = row[x-1] if x > 0 else 1
        row.append(prevColValue + prevRow[x])
    grid.append(row)

print grid
print "Result: ", grid[height-1][width-1]
