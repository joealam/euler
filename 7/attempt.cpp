#include <iostream>
#include <climits>

using namespace std;

bool isPrime( long );
long nextPrime( long );

bool isPrime( long n )
{
	for ( int i = 2; i < n; i++ )
	{
		if ( ( n % i ) == 0 )
		{
			return false;
		}
	}
	return true;
}

long nextPrime( long start )
{
	int i = start + 1;
	while ( i < LONG_MAX )
	{
		if ( isPrime( i ) ) return i;
		i++;
	}
	return -1;
}

int main( int argc, char* argv[] )
{
	const int prime_no = 10001;
	int current = 1;

	for ( int i = 0; i < prime_no; i++ )
	{
		cout << "\r" << (current = nextPrime( current )) << "\t\t";
	}

	cout << "Solution: " << current << endl;
	return 0;
}
