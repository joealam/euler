/* CountDivisors.h
 * Declares the CountDivisors function
 */
#ifndef _COUNTDIVISORS_H_
#define _COUNTDIVISORS_H_

#include <vector>

const int INVALID_NUMBER = -1;
const int NEED_MORE_PRIMES = -2;

long CountDivisors( const long number, const std::vector<long>& primes );

#endif