#include "GeneratePrimes.h"

using std::vector;

void GeneratePrimes( vector<long>* dest, unsigned long max )
{
	bool* marked = new bool[max];
	for ( long i = 0; i < max; i++ )
	{
		marked[i] = false;
	}

	// Check numbers
	long p = 2;
	while ( p < max )
	{
		long start_p = p;

		for ( long i = p; (p * i) < max; i++ )
		{
			//cout << "Marking: " << i << endl;
			marked[p * i] = true;
		}

		for ( long i = p; i < max; i++ )
		{
			if ( marked[i + 1] == false )
			{
				p = i + 1;
				break;
			}
		}
		if ( p == start_p ) break;
	}

	for ( long i = 2; i < max; i++ )
	{
		if ( marked[i] == false )
		{
			dest->push_back( i );
		}
	}
	delete[] marked;
}
