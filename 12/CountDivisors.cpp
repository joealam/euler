/* CountDivisors.cpp
 *
 * Defines the CountDivisors function
 */
#include "CountDivisors.h"
#include <algorithm>

using std::vector;
using std::binary_search;

long CountDivisors( const long number, const vector<long>& primes )
{
	// Check the number is a real number before starting, also return 1 if it is 1 (not really worth calculating)
	if ( number < 1 ) return INVALID_NUMBER;
	if ( number == 1 ) return 1;
	
	// If the number is prime, return 2
	if ( binary_search( primes.begin(), primes.end(), number ) )
	{
		return 2;
	}

	long n = number;

	// Create an iterator for the prime number and start at the beginning
	vector<long>::const_iterator primeIt = primes.begin();

	long divisorCount = 1;

	while ( n > 1 )
	{
		int exponent  = 0;

		// If n is directly divisible by the current prime, increment the exponent counter
		while ( n % *primeIt == 0 )
		{
			n /= *primeIt;
			exponent++;
		}

		if ( exponent )
		{
			divisorCount *= exponent + 1;
		}

		// Increment prime iterator
		if ( ++primeIt == primes.end() )
		{
			// Ran out of prime numbers
			return NEED_MORE_PRIMES;
		}

	}

	return divisorCount;
}