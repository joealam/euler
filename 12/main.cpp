#include <iostream>
#include <vector>
#include <algorithm>
#include "GeneratePrimes.h"
#include "CountDivisors.h"
#include <Windows.h>

using namespace std;

int main( int argc, char* argv[] )
{
	cout << "-- Begin Generating Prime Numbers -- " << endl;
	int ticks = GetTickCount();

	// Generate primes
	vector<long> primes;
	GeneratePrimes( &primes, 100000 );

	cout << "-- Prime Numbers Generated in: " << GetTickCount() - ticks << " ticks --" << endl;
	//cin.get();

	cout << "-- Begin Calculating Triangle Number Divisors -- " << endl;
	ticks = GetTickCount();

	const int max = 15000; // Maximum triangle number to calculate (nth triangle number)
	const int target_divisors = 500; // Number of divisors to find a number with more than
	const bool verbose = false; // Whether or not the program should dump everything to console during calculations

	int n = 1;

	while ( n < max )
	{
		long triangle = n * (n+1) / 2;
		if ( verbose )
		{
			cout.fill( ' ' );
			cout.width( 10 );
			cout << "Calculating: \t" << triangle;
		}
		long divisors = CountDivisors( triangle, primes );

		// Catch errors
		if ( divisors == NEED_MORE_PRIMES )
		{
			cout << endl << "There are not enough prime numbers generated. Higher than " << primes.back() << " is needed." << endl;
			break;
		}
		else if ( divisors == INVALID_NUMBER )
		{
			cout << endl << "The number " << triangle << " is not a valid number for the function (must be +ve)" << endl;
			break;
		}

		if ( verbose )
		{
			cout << "\tDivisors: " << divisors << endl;
		}
		if ( divisors > target_divisors )
		{
			cout << "Result: " << triangle << endl;
			break;
		}

		// Calculate for the next number
		n++;
	}
	cout << "-- End Calculating Triangle Number Divisors. Took: " << GetTickCount() - ticks << " ticks -- " << endl;

	cin.get();
	return 0;
}
