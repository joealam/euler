#include <iostream>

using namespace std;

int main( int argc, char* argv[] )
{
	const int check_upto = 20;
	const int start = check_upto * (check_upto-1);

	int n = start;
	while ( true )
	{
		cout << n << endl;
		bool works = true;
		for ( int i = 1; i < check_upto; i++ )
		{
			if ( (n % i) != 0 )
			{
				works = false;
				break;
			}
		}
		if ( works ) break;
		else n += check_upto;
	}
	cout << "The solution is " << n << endl;

	return 0;
}
