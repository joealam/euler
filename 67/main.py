# Project Euler Problem #18 and Project Euler Problem #67
# https://projecteuler.net/problem=18 and https://projecteuler.net/problem=67
# The concept here is to treat the number triangle as a binary tree, as the problem states that only adjacent moves
# are allowed.
# Once treated as a binary tree, the maximum value that any node could represent may be calculated by summing itself
# with its largest child. Once this has been repeated on all nodes, starting at the leaves all the way to the root, the
# value of the root node will be the maximum possible attainable value

# Problem #18 Numbers
number_triangle = [
    [75],
    [95, 64],
    [17, 47, 82],
    [18, 35, 87, 10],
    [20, 04, 82, 47, 65],
    [19, 01, 23, 75, 03, 34],
    [88, 02, 77, 73, 07, 63, 67],
    [99, 65, 04, 28, 06, 16, 70, 92],
    [41, 41, 26, 56, 83, 40, 80, 70, 33],
    [41, 48, 72, 33, 47, 32, 37, 16, 94, 29],
    [53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14],
    [70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57],
    [91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48],
    [63, 66, 04, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31],
    [04, 62, 98, 27, 23, 9, 70, 98, 73, 93, 38, 53, 60, 04, 23] # WTF is up with this line? Replacing 9 with 09 kills it
]

# Problem #67 numbers are read from a file and ensure they get converted to integers
number_triangle = []
for line in open("p067_triangle.txt").readlines():
    row = []
    for valueString in line.replace("\n", "").split(" "):
        row.append(int(valueString))
    number_triangle.append(row)


# For each row (starting one above the bottom)
for rowIndex in range(len(number_triangle) - 2, -1, -1):
    # Replace each node's value with its maximum possible value
    for colIndex in range(len(number_triangle[rowIndex])):
        number_triangle[rowIndex][colIndex] += max(number_triangle[rowIndex+1][colIndex], number_triangle[rowIndex+1][colIndex+1])

print "Result: ", number_triangle[0][0]